﻿namespace Config;

public sealed record AppConfiguration
(
    string PgConnectionString,
    string RabbitMQHost = "localhost",
    int RabbitMQPort = 5672,
    string RabbitMQUser = "guest",
    string RabbitMQPassword = "guest",
    string RabbitMQQueues = "measurements",
    string RabbitMQVHost = "dev",
    int RabbitMQListeners = 3,
    ushort RabbitMQPrefetch = 250,
    string HealthCheckFilePath = "healthy.txt",
    int HealthCheckIntervalSec = 5
)
{
    public static readonly string AppConfigurationPrefix = "PanelEmotoAgh_";
    public string[] RabbitMQQueuesArr => RabbitMQQueues.Split(",");
}
