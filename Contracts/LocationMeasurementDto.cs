namespace Contracts;

public record LocationMeasurementDto(double Lon, double Lat, int MachineId, int MeasurementTypeId, DateTime Time): MeasurementDto(MachineId, MeasurementTypeId, Time)
{
    public const string MessageType = "location";
    private const int MaxLat = 90;
    private const int MinLat = -90;
    private const int MaxLon = 180;
    private const int MinLon = -180;

    public override bool ValidateDTO()
        => base.ValidateDTO() && 
            MaxLat >= Lat && 
            Lat >= MinLat && 
            MaxLon >= Lon && 
            Lon >= MinLon;
}