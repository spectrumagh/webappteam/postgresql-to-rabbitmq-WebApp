namespace Contracts;

public abstract record MeasurementDto(int MachineId, int MeasurementTypeId, DateTime Time)
{
    public virtual bool ValidateDTO()
        => Time >= DateTime.MinValue;
}
