namespace Contracts;

public record StringMeasurementDto(string Value, int MachineId, int MeasurementTypeId, DateTime Time): MeasurementDto(MachineId, MeasurementTypeId, Time)
{
    public const string MessageType = "string";

    public override bool ValidateDTO()
        => base.ValidateDTO() && Value != null;
}