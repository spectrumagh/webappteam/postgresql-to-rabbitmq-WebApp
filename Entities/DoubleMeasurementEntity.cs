﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("DoubleMeasurement")]
public class DoubleMeasurementEntity : MeasurementEntity<double> { }
