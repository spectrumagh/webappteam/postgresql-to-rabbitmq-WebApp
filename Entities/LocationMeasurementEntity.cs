﻿using NetTopologySuite.Geometries;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("LocationMeasurement")]
public class LocationMeasurementEntity : MeasurementEntity<Point>
{
    [Column(TypeName = "geometry (point)")]
    public override required Point Value { get; set; }
}
