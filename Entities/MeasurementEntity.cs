﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Entities;

/// <summary>
/// A class that has an explicitly typed measurement value
/// </summary>
/// <typeparam name="T">Measurement value type</typeparam>
public class MeasurementEntity<T> : MeasurementEntity
{
    public MeasurementEntity() : base(typeof(T)) { }

    /// <summary>
    /// Measurement value
    /// </summary>
    public required virtual T Value { get; set; }
}

/// <summary>
/// A class with no measurement value.
/// It can be used to query different tables that have different types of measurements
/// </summary>
public abstract class MeasurementEntity : Entity
{
    internal protected MeasurementEntity(Type dataType) => DataType = dataType;

    /// <summary>
    /// Measurement data type ex. <c>double</c>, <c>Point</c> used in class 
    /// </summary>
    [JsonIgnore]
    [NotMapped]
    public Type DataType { get; protected set; }

    /// <summary>
    /// Measurement primary key
    /// </summary>
    [Key]
    public virtual int MeasurementId { get; set; }

    /// <summary>
    /// Measurement type Id
    /// </summary>
    public virtual int MeasurementTypeId { get; set; }

    /// <summary>
    /// Measurement recording time.
    /// </summary>
    public virtual DateTime Time { get; set; }

    /// <summary>
    /// Machine Id foregin key
    /// </summary>
    public virtual int MachineId { get; set; }
}
