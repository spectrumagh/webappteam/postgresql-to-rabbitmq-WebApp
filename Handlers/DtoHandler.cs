using Contracts;

namespace Handlers;

public class DtoHandler(
    StringMeasurementsHandler stringMeasurementsHandler,
    LocationMeasurementsHandler locationMeasurementsHandler)
{
    private static T ParseJson<T>(string rawJson) where T : class => System.Text.Json.JsonSerializer.Deserialize<T>(rawJson) ?? throw new Exception("");

    public async Task HandleDTO(string messageType, int routingKey, string jsonString, CancellationToken cancellationToken)
    {
        var handlerTask = messageType switch
        {
            StringMeasurementDto.MessageType => stringMeasurementsHandler.HandleAsync(ParseJson<StringMeasurementDto>(jsonString), routingKey, cancellationToken),
            LocationMeasurementDto.MessageType => locationMeasurementsHandler.HandleAsync(ParseJson<LocationMeasurementDto>(jsonString), routingKey, cancellationToken),
            _ => throw new NotSupportedException( messageType + " is not supported message type" ),
        };
        await handlerTask;
    }
}
