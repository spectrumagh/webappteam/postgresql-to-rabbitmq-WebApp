using Contracts;
using Entities;
using NetTopologySuite.Geometries;
using Repository;

namespace Handlers;

public class LocationMeasurementsHandler(
    IMeasurementRepository<LocationMeasurementEntity> measurementRepository) : MeasurementsHandler<LocationMeasurementDto, LocationMeasurementEntity>(measurementRepository)
{
    protected override LocationMeasurementEntity MapDtoToEntity(LocationMeasurementDto dto, int routingKey) 
        => new()
            {
                MachineId = routingKey,
                MeasurementTypeId = dto.MeasurementTypeId,
                Time = dto.Time,
                Value = new Point(dto.Lat, dto.Lon)
            };
}