using Contracts;
using Entities;
using Repository;

namespace Handlers;

public abstract class MeasurementsHandler<TDto, TEntity>(
    IMeasurementRepository<TEntity> measurementRepository)
    where TEntity : MeasurementEntity
    where TDto : MeasurementDto
{
    public async Task HandleAsync(TDto dto, int routingKey, CancellationToken cancellationToken)
    {
        if (!dto.ValidateDTO())
        {
            throw new InvalidOperationException("Dto is not valid.");
        }

        try
        {
            var entity = MapDtoToEntity(dto, routingKey);
            await measurementRepository.InsertAsync(entity, cancellationToken);
            await measurementRepository.CommitAsync(cancellationToken);
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException("Error occurred while processing and saving entity to the database.", ex);
        }
    }

    protected abstract TEntity MapDtoToEntity(TDto dto, int routingKey);
}