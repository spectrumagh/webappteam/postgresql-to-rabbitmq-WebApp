using Contracts;
using Entities;
using Repository;

namespace Handlers;

public class StringMeasurementsHandler(
    IMeasurementRepository<DoubleMeasurementEntity> measurementRepository) : MeasurementsHandler<StringMeasurementDto, DoubleMeasurementEntity>(measurementRepository)
{
    protected override DoubleMeasurementEntity MapDtoToEntity(StringMeasurementDto dto, int routingKey)
        => new()
            {
                MachineId = routingKey,
                MeasurementTypeId = dto.MeasurementTypeId,
                Time = dto.Time,
                Value = double.Parse(dto.Value)
            };
}