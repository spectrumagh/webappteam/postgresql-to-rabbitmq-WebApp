using Config;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace HealthChecks;

public class HealthCheckFilePublisher(
    AppConfiguration appConfig, 
    ILogger<HealthCheckFilePublisher> logger) : IHealthCheckPublisher
{
    public Task PublishAsync(HealthReport report, CancellationToken cancellationToken)
    {
        if (report.Status == HealthStatus.Healthy)
        {
            File.WriteAllText(appConfig.HealthCheckFilePath, $"{DateTime.UtcNow}: Healthy");
        }
        else
        {
            logger.LogWarning($"Health Check Status: {report.Status}");
            EnsureDelete(appConfig.HealthCheckFilePath);
        }
        return Task.CompletedTask;
    }

    internal static void EnsureDelete(string filePath)
    {
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }
}
