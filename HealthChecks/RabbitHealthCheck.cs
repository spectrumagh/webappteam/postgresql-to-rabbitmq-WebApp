using Microsoft.Extensions.Diagnostics.HealthChecks;
using Services;

namespace HealthChecks;

public class RabbitHealthCheck(
    RabbitMQConnectionManager rabbitConnectionManager) : IHealthCheck
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        try
        {
            var isRabbitHealthy = rabbitConnectionManager.Channel?.IsOpen ?? throw new ArgumentNullException(nameof(rabbitConnectionManager.Channel));
            
            if (isRabbitHealthy)
            {
                return Task.FromResult(HealthCheckResult.Healthy($"{DateTime.UtcNow}: Healthy"));
            }
            return Task.FromResult(HealthCheckResult.Unhealthy($"{DateTime.UtcNow}: Unhealthy - RabbitMQ: {isRabbitHealthy}"));
        }
        catch (Exception ex)
        {
            return Task.FromResult(HealthCheckResult.Unhealthy("Error occurred.", ex));
        }
    }
}