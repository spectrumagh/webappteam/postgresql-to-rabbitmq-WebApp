﻿using Microsoft.EntityFrameworkCore;
using Entities;
using Microsoft.EntityFrameworkCore.Design;

namespace Persistence;

public class AppDesignTimeDbContextFactor : IDesignTimeDbContextFactory<AppDbContext>
{
    public AppDbContext CreateDbContext(string[] args)
    {
        var options = new DbContextOptionsBuilder<AppDbContext>();
        options.UseNpgsql("", x => x.UseNetTopologySuite());
        return new AppDbContext(options.Options);
    }
}

public class AppDbContext : DbContext
{
    public DbSet<DoubleMeasurementEntity> Parameters { get; set; }
    public DbSet<LocationMeasurementEntity> LocationParameter { get; set; }
    
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.HasPostgresExtension("postgis");
        base.OnModelCreating(builder);
    }

    // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //     => optionsBuilder.UseLazyLoadingProxies(true);

    /// <summary>
    /// To set the proper DateTime Kind.
    /// Note that the database doesn't store the time zone 
    /// </summary>    
    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        => configurationBuilder
            .Properties<DateTime>()
            .HaveConversion<DateTimeUTCDbContextConverter>();
}
