using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Persistence;

/// <summary>
/// The default DateTime form from the database is Undefined, but to work with the database it should be set to <c>DateTimeKind.Utc</c>
/// </summary>
public sealed class DateTimeUTCDbContextConverter : ValueConverter<DateTime, DateTime>
{
    public DateTimeUTCDbContextConverter()
        : base(
        v => v.ToUniversalTime(),
        v => v.ToUniversalTime())
    { }
}
