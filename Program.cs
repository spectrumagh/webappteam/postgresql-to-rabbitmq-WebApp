using Microsoft.EntityFrameworkCore;
using Persistence;
using Config;
using Services;
using Handlers;
using Entities;
using Repository;
using HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

var tempConfigBuilder = new ConfigurationBuilder();
tempConfigBuilder
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
    .AddUserSecrets<Program>()
    .AddCommandLine(args);
var appConfiguration = tempConfigBuilder.Build().Get<AppConfiguration>()
    ?? throw new ArgumentNullException(nameof(tempConfigBuilder));


var host = Host.CreateDefaultBuilder(args);

host.ConfigureServices(svc =>
{
    svc.AddSingleton(appConfiguration);
    svc.AddSingleton<RabbitMQConnectionManager>();
    svc.AddDbContext<AppDbContext>(options => options.UseNpgsql(appConfiguration.PgConnectionString, x => x.UseNetTopologySuite()), ServiceLifetime.Singleton);

    svc.AddHealthChecks().AddCheck<RabbitHealthCheck>("rabbitmq_health_check").AddDbContextCheck<AppDbContext>();

    svc.AddScoped<IMeasurementRepository<DoubleMeasurementEntity>, MeasurementRepository<DoubleMeasurementEntity>>();
    svc.AddScoped<IMeasurementRepository<LocationMeasurementEntity>, MeasurementRepository<LocationMeasurementEntity>>();

    svc.AddSingleton<StringMeasurementsHandler>();
    svc.AddSingleton<LocationMeasurementsHandler>();
    svc.AddSingleton<DtoHandler>();

    svc.AddHostedService<RabbitMQHostedService>();

    svc.Configure<HealthCheckPublisherOptions>(options =>
    {
        options.Period = TimeSpan.FromSeconds(appConfiguration.HealthCheckIntervalSec);
        options.Predicate = _ => true;
    });
    svc.AddSingleton<IHealthCheckPublisher, HealthCheckFilePublisher>();
});

await host.Build().RunAsync();