# WIP

## compiled ef models

https://www.learnentityframeworkcore.com/misc/compiled-models

```bash
dotnet ef dbcontext optimize --namespace Persistence.CompiledModels
```

protection level https://github.com/npgsql/efcore.pg/issues/2972