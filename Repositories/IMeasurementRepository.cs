﻿using Entities;

namespace Repository;

public interface IMeasurementRepository<TMeasurement>
    where TMeasurement : MeasurementEntity
{
    public Task InsertAsync(TMeasurement entity, CancellationToken cancellationToken);
    public Task<int> CommitAsync(CancellationToken cancellationToken);
}