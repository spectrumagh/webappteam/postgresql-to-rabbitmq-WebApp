using Entities;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Repository;

public class MeasurementRepository<TMeasurement> : IMeasurementRepository<TMeasurement>
    where TMeasurement : MeasurementEntity
{
    private readonly AppDbContext _dbContext;
    private readonly DbSet<TMeasurement> _dbSet;

    public MeasurementRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
        _dbSet = _dbContext.Set<TMeasurement>();
    }

    public async Task InsertAsync(TMeasurement entity, CancellationToken cancellationToken)
        => await _dbSet.AddAsync(entity, cancellationToken);

    public async Task<int> CommitAsync(CancellationToken cancellationToken)
        => await _dbContext.SaveChangesAsync(cancellationToken);
}
