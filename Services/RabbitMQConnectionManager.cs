using RabbitMQ.Client;

namespace Services;

public class RabbitMQConnectionManager()
{
    public IConnection? Connection { get; set; }
    public IModel? Channel { get; set; }
}