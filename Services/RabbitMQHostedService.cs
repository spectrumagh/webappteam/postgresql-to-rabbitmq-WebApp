using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Handlers;
using Config;

namespace Services;

public class RabbitMQHostedService(
    RabbitMQConnectionManager rabbitConnectionManager,
    AppConfiguration appConfig,
    ILogger<RabbitMQHostedService> logger,
    DtoHandler dtoHandler) : IHostedService
{
    private readonly ConnectionFactory _connectionFactory = new()
    {
        HostName = appConfig.RabbitMQHost,
        Port = appConfig.RabbitMQPort,
        UserName = appConfig.RabbitMQUser,
        Password = appConfig.RabbitMQPassword,
        VirtualHost = appConfig.RabbitMQVHost,
        DispatchConsumersAsync = true,
        ConsumerDispatchConcurrency = 1,
    };

    public Task StartAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("RabbitMQHostedService is starting.");

        rabbitConnectionManager.Connection = _connectionFactory.CreateConnection();
        rabbitConnectionManager.Channel = rabbitConnectionManager.Connection.CreateModel();

        foreach (var queue in appConfig.RabbitMQQueuesArr)
        {
            subscribeQueue(rabbitConnectionManager.Channel, queue, cancellationToken);
        }

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("RabbitMQHostedService is stopping.");

        rabbitConnectionManager.Channel?.Close();
        rabbitConnectionManager.Connection?.Close();

        return Task.CompletedTask;
    }

    internal void subscribeQueue(IModel channel, string queue, CancellationToken cancellationToken) 
    {
        var consumer = new AsyncEventingBasicConsumer(channel);
        consumer.Received += (_, eventArgs) => receivedCallback(channel, eventArgs, dtoHandler, cancellationToken);

        channel.BasicQos(0, appConfig.RabbitMQPrefetch, false);
        channel.BasicConsume(queue: queue, autoAck: false, consumer: consumer);

        logger.LogInformation($"Listening to RabbitMQ queue: {queue}");
    }

    internal async Task receivedCallback(IModel channel, BasicDeliverEventArgs eventArgs, DtoHandler dtoHandler, CancellationToken cancellationToken)
    {
        try
        {
            var routingKey = int.Parse(eventArgs.RoutingKey);
            await dtoHandler.HandleDTO(eventArgs.BasicProperties.Type, routingKey, Encoding.UTF8.GetString(eventArgs.Body.Span), cancellationToken);
            channel.BasicAck(eventArgs.DeliveryTag, false);
        }
        catch (Exception ex)
        {
            logger.LogError($"{ex.Message}", ex);
            channel.BasicNack(eventArgs.DeliveryTag, false, false);
        }
    }
}
