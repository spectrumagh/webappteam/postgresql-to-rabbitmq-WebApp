#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

# arm64 - arm64v8, 
# amd64 - amd64
ARG PLATFORM=amd64

FROM mcr.microsoft.com/dotnet/aspnet:8.0.1-alpine3.19-$PLATFORM AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:8.0.101-jammy-$PLATFORM AS build
WORKDIR /src
COPY ["rabbit-to-postgresql.csproj", "."]
RUN dotnet restore "rabbit-to-postgresql.csproj"
COPY . .

FROM build AS publish
RUN dotnet publish "rabbit-to-postgresql.csproj" -c Release -o /app/publish --no-restore

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "rabbit-to-postgresql.dll"]